# Common DevOps variables and functions
[[ "$TRACE" ]] && set -x

if [ ! -z "$HELM_PROJECT_PREFIX" ]; then
  export HELM_PROJECT_PREFIX="${HELM_PROJECT_PREFIX}-"
fi

export KUBE_NAMESPACE=${KUBE_NAMESPACE_CUSTOM:-$HELM_PROJECT_PREFIX$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG}
export HELM_RELEASE=${HELM_RELEASE:-$HELM_PROJECT_PREFIX$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG}
# "ops" folder directory path.
HELM_OPS_DIR=${HELM_OPS_DIR:-"ops"}
export HELM_OPS_DIR=${HELM_OPS_DIR%/}
# helm chart directory path
HELM_CHART_DIR=${HELM_CHART_DIR:-"$HELM_OPS_DIR/charts/$CI_PROJECT_NAME"}
export HELM_CHART_DIR=${HELM_CHART_DIR%/}
export HELM_DOCKER_REPOSITORY=${HELM_DOCKER_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
export HELM_DOCKER_TAG=${HELM_DOCKER_TAG:-$CI_COMMIT_SHA}
# Dockerfile directory path
HELM_DOCKERFILE_DIR=${HELM_DOCKERFILE_DIR:-"."}
export HELM_DOCKERFILE_DIR=${HELM_DOCKERFILE_DIR%/}

echo "CI_PROJECT_NAME: ${CI_PROJECT_NAME}"
echo "CI_ENVIRONMENT_SLUG: ${CI_ENVIRONMENT_SLUG}"
echo "CI_ENVIRONMENT_URL: ${CI_ENVIRONMENT_URL}"
echo "CI_COMMIT_SHA: ${CI_COMMIT_SHA}"
echo "CI_COMMIT_TAG: ${CI_COMMIT_TAG}"
echo "CI_PIPELINE_ID: ${CI_PIPELINE_ID}"
echo "CI_JOB_ID: ${CI_JOB_ID}"
echo "CI_JOB_NAME: ${CI_JOB_NAME}"
echo "CI_REGISTRY_USER: ${CI_REGISTRY_USER}"
echo "CI_REGISTRY: ${CI_REGISTRY}"
echo "HELM_PROJECT_PREFIX: ${HELM_PROJECT_PREFIX}"
echo "HELM_DOCKER_REPOSITORY: ${HELM_DOCKER_REPOSITORY}"
echo "HELM_DOCKERFILE_DIR: ${HELM_DOCKERFILE_DIR}"
echo "HELM_DOCKER_TAG: ${HELM_DOCKER_TAG}"
echo "HELM_RELEASE: ${HELM_RELEASE}"
echo "HELM_CHART_DIR: ${HELM_CHART_DIR}"
echo "HELM_CONFIG_REPO: ${HELM_CONFIG_REPO}"
echo "HELM_OPS_DIR: ${HELM_OPS_DIR}"
echo "KUBE_NAMESPACE: ${KUBE_NAMESPACE}"

function utils_create_kubeconfig() {
    echo "Generating kubeconfig..."

    export KUBECONFIG="$(pwd)/kubeconfig"
    # IF TEST_KUBECONFIG is set and not on "master" use TEST_KUBECONFIG
    if [ "$CI_COMMIT_REF_NAME" != "master" ]; then
      echo "Using TEST K8S cluster"
      if [ -z "$TEST_KUBECONFIG" ]; then
        echo "Error: TEST K8S cluster config not found in CI variables. Please define TEST_KUBECONFIG file variable."
          exit 155
      fi
      cat "$TEST_KUBECONFIG" | base64 -d > "$(pwd)/kubeconfig"
    # otherwise PROD_KUBECONFIG
    else
      echo "Using PROD K8S cluster"
      if [ -z "$PROD_KUBECONFIG" ]; then
        echo "Error: PROD K8S cluster config not found in CI variables. Please define PROD_KUBECONFIG file variable."
          exit 155
      fi
      cat "$PROD_KUBECONFIG" | base64 -d > "$(pwd)/kubeconfig"
    fi
    chmod 600 $(pwd)/kubeconfig
    echo "Finished generating kubeconfig..."
}

function utils_install_dependencies() {
  echo "Installing install_dependencies..."
  echo "downloading and installing Helm..."
  curl -s "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
  mv linux-amd64/helm /usr/bin/
  helm version | sed 's/^/[Helm version:] /'
  echo "finished downloading and installing Helm..."
  
  echo "downloading and installing kubectl"
  curl -Lso /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
  chmod +x /usr/bin/kubectl
  kubectl version --client | sed 's/^/[Kubectl client version:] /'
  echo "finished downloading and installing kubectl"
}


function utils_setup_docker() {
  if command -v docker 2>/dev/null; then
    if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
      export DOCKER_HOST='tcp://localhost:2376'
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384
    for i in $(seq 1 30)
      do
        docker info &>/dev/null && break
        echo 'Waiting for docker to start'
        sleep 1s
    done
    fi
  fi
}


function utils_prepare_chart() {
  echo "Preparing chart dependencies..."
  
  if [ -d "${HELM_CHART_DIR}" ]; then 
      helm dependency update ${HELM_CHART_DIR}/
      helm dependency build ${HELM_CHART_DIR}/
      echo "Finished preparing chart dependencies..."
    else
      echo "Directory named in HELM_CHART_DIR doesn't exists. Helm dependency skipped!"
  fi
}


function utils_ensure_namespace() {
  echo "Setting up namespace..."
  echo "Using KUBECONFIG: $KUBECONFIG"
  echo "Setting namespace to: ${KUBE_NAMESPACE}"
  kubectl get namespace --kubeconfig=${KUBECONFIG} "$KUBE_NAMESPACE" 2>&1 && echo "namespace $KUBE_NAMESPACE already exists" || \
      kubectl create --kubeconfig=${KUBECONFIG} namespace "$KUBE_NAMESPACE"
  echo "Finished setting up namespace"
}


function utils_check_kube_domain() {
  if [ -z ${HELM_APP_BASE_DOMAIN} ]; then
    echo "In order to deploy or use Review Apps, HELM_APP_BASE_DOMAIN variable must be set"
    echo "You can do it in Auto DevOps project settings or defining a secret variable at group or project level"
    echo "You can also manually add it in .gitlab-ci.yml"
    # false
  else
    echo "HELM_APP_BASE_DOMAIN: ${HELM_APP_BASE_DOMAIN}"
    true
  fi
}


function utils_docker_build() {
  echo "Preparing docker image..."
  if [[ -n "$CI_REGISTRY" && -n "$CI_REGISTRY_USER" ]]; then
    echo "Logging to GitLab Container Registry with CI credentials..."
    echo "$CI_REGISTRY_PASSWORD" | docker login \
      --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
  fi

  # Skip build, if image already exists in registry
  docker pull -q  "$HELM_DOCKER_REPOSITORY:$HELM_DOCKER_TAG" \
    && echo "docker image $HELM_DOCKER_REPOSITORY:$HELM_DOCKER_TAG already exists. Skipping build" \
    && exit 0
  
  if [[ -f "${HELM_DOCKERFILE_DIR}/Dockerfile" ]]; then
    echo "Starting docker build..."
    if [ -z "$CI_COMMIT_TAG" ]; then 
      docker build -t "$HELM_DOCKER_REPOSITORY:$HELM_DOCKER_TAG" "${HELM_DOCKERFILE_DIR}"
      echo "Finished build"
      echo "Pushing to GitLab Container Registry..."
      docker push "$HELM_DOCKER_REPOSITORY:$HELM_DOCKER_TAG"
    else 
      docker build -t "$HELM_DOCKER_REPOSITORY:$HELM_DOCKER_TAG" \
        -t "$HELM_DOCKER_REPOSITORY:$CI_COMMIT_TAG" "${HELM_DOCKERFILE_DIR}"
      echo "Finished build"
      echo "Pushing to GitLab Container Registry..."
      docker push "$HELM_DOCKER_REPOSITORY:$HELM_DOCKER_TAG"
      docker push "$HELM_DOCKER_REPOSITORY:$CI_COMMIT_TAG"
    fi
  else
    echo "No Dockerfile found!"
    exit 156
  fi

  echo "Finished to build and push images to container registry"
}

function utils_create_registry_json() {
  echo "Creating docker registry file..."
  # CI_DEPLOY_USER & $CI_DEPLOY_PASSWORD are automatically populated, if there
  # is depoloy token configured in CI with name "gitlab-deploy-token"  
  export GITLAB_AUTH=$(echo -n "${CI_DEPLOY_USER}:${CI_DEPLOY_PASSWORD}" | base64 -w0)
  cat "$DOCKERHUB_API_KEY" | base64 -d | jq --arg pass $GITLAB_AUTH --arg registry $CI_REGISTRY '.auths += {($registry):{"auth": $pass}}' > $(pwd)/docker_registry_config.json
}

function utils_create_registry_secret() {
  echo "Creating registry secret gitlab-registry containing multiple registries..."
  kubectl create --kubeconfig="${KUBECONFIG}" secret -n "$KUBE_NAMESPACE" generic gitlab-registry \
      --from-file=.dockerconfigjson=$(pwd)/docker_registry_config.json \
      --type=kubernetes.io/dockerconfigjson \
      -o yaml --dry-run=client | kubectl replace --kubeconfig="${KUBECONFIG}" -n "$KUBE_NAMESPACE" --force -f -
}

function utils_delete() {
  echo "Deleting environment..."
  if [[ -n "$(helm list -q --filter "^${HELM_RELEASE}$" --namespace "${KUBE_NAMESPACE}")" ]]; then
    echo "Uninstalling release ${HELM_RELEASE} from namespace ${KUBE_NAMESPACE} ..."
    helm uninstall --kubeconfig "$KUBECONFIG" --namespace "${KUBE_NAMESPACE}" "${HELM_RELEASE}"
    echo "Finished uninstalling helm release"
    echo "Deleteing namespace ${KUBE_NAMESPACE}"
    kubectl delete --kubeconfig="${KUBECONFIG}" namespaces "${KUBE_NAMESPACE}"
    echo "Finished deleting namespace"
  fi
  echo "Finished deleting environment..."
}


function utils_dast() {
  export CI_ENVIRONMENT_URL=$(cat environment_url.txt)
  echo "Generating Dynamic Application Security Testing report"
  mkdir /zap/wrk/
  /zap/zap-baseline.py -J dast-report.json -r dast-report.html -t "$CI_ENVIRONMENT_URL" || true
  cp /zap/wrk/dast-report.json .
  cp /zap/wrk/dast-report.html .
  echo "Finished generating Dynamic Application Security Testing report"
}


function utils_persist_environment_url() {
    if [ ! -z "${CI_ENVIRONMENT_URL}" ]; then 
      echo "$CI_ENVIRONMENT_URL" > environment_url.txt
    fi
}

function utils_slack_environment_send() {
  echo "Sending notification to Slack..."
  if [ ! -z "${SLACK_PAYLOAD}" ] || [ ! -z "${SLACKURL}" ]; then
    SUBSTITUTED_PAYLOAD="$(echo "$SLACK_PAYLOAD" | envsubst)"
    STATUS_CODE=$(curl \
            --write-out %{http_code} \
            --silent \
            --output /dev/null \
            -X POST \
            -H 'Content-type: application/json' \
            --data "${SUBSTITUTED_PAYLOAD}" ${SLACKURL})

    echo "Slack API returned ${STATUS_CODE} code"
    echo "Finished sending notification to Slack..."
  fi
}

function utils_import_configs() {
  echo "Cloning deployment configs from configs repository..."
  git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/${HELM_CONFIG_REPO}.git helm-config
  echo "Finished cloning deployment configs from configs repository..."
  echo "Copying _override files from configs repository"
  # Use CI_ENVIRONMENT_NAME or fallback to CI_COMMIT_BRANCH to construct configs_folder
  if [ ! -z "${CI_ENVIRONMENT_NAME}" ]; then 
    echo "Using CI_ENVIRONMENT_NAME as path for overrides."
    configs_folder=$(echo $CI_ENVIRONMENT_NAME | cut -d/ -f1 )
  elif [ ! -z "${CI_COMMIT_BRANCH}"]; then 
    echo "Using CI_COMMIT_BRANCH as path for overrides."
    configs_folder=$(echo $CI_COMMIT_BRANCH | cut -d/ -f1 )
  fi
  vars_override_configs="helm-config/${CI_PROJECT_NAME}/${configs_folder}/vars_override.sh"
  values_override_configs="helm-config/${CI_PROJECT_NAME}/${configs_folder}/values_override.yml"
  if [ -f "$vars_override_configs" ]; then
    echo "vars_override.sh from configs repo found. Overriding..."
    cat $vars_override_configs > "${HELM_OPS_DIR}"/vars_override.sh
  fi
  if [ -f "$values_override_configs" ]; then
    echo "values_override.yml from configs repo found. Overriding..."
    cat $values_override_configs > "${HELM_OPS_DIR}"/values_override.yml
  fi
  echo "Finished copying _override files from configs repository"
  echo "Detecting job variables that contains overrides"
  if [ ! -z "${vars_override}" ]; then
    echo "vars_override variable detected passed to CI job. Overriding..."
    echo -n "${vars_override}" | base64 -d > "${HELM_OPS_DIR}"/vars_override.sh
  fi
  if [ ! -z "${values_override}" ]; then 
    echo 'values_override variable detected passed to CI job. Overriding...'
    echo -n "${values_override}" | base64 -d > "${HELM_OPS_DIR}"/values_override.yml
  fi
}

function utils_prepare_deploy() {
  utils_check_kube_domain
  utils_install_dependencies
  utils_create_kubeconfig
  utils_prepare_chart
  utils_ensure_namespace
  utils_create_registry_json || true
  utils_create_registry_secret || true
}

function utils_default_deploy() {
  echo "Starting standard deploy process..."
  echo "Deploying $CI_PROJECT_NAME $CI_JOB_NAME..."
  utils_prepare_deploy
  utils_import_configs
  chmod -R +x "${HELM_OPS_DIR}"
  "${HELM_OPS_DIR}"/install.sh
  utils_slack_environment_send
  utils_persist_environment_url
  echo "Finished standard deploy process..."
}
